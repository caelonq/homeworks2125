import java.io.*;
import java.util.*;


public class FileCompiler
{


	private String filename = "";
	private Scanner input = new Scanner(System.in);
	private String [] filenames;
	private String file = "newResult.txt";
	private StringBuilder content = new StringBuilder();
	private BufferedWriter writer;

	public FileCompiler()
	{
		sendToFile(catAll(fileRequest()), this.file);
		
	
	}

	public String fileRequest()
	{
    	System.out.print("Enter File: ");
    	this.filename = input.nextLine();
    	return this.filename;
	}

	public StringBuilder catAll(String file)
	{	
		String line = "";
		File temp = null;
		filenames = null;
		BufferedReader reader = null;
		try
		{	
			 reader = new BufferedReader(new FileReader(file));
		}
		catch(IOException e)
		{
		  System.out.println("Error Opening File "+ file.toString());
		  e.printStackTrace(); 
		}
		try
		{
				
				while((line = reader.readLine()) != null)
				{		
						
					filenames = line.split("\\s");	
					while(filenames[0].matches("\\s*#Include|#include"))
					{	
						filenames = filenames[1].split("\"");

						temp = new File(new File(filenames[1]).getAbsoluteFile().toString().replace("..\\", ""));

						if(!(temp.equals(new File(this.filename).getAbsoluteFile())))
						{	
							catAll(temp.toString());
						}

						line = reader.readLine();
						filenames = line.split("\\s");	

					}
				 	
						content.append("\n");
						content.append(line);						
				}	

		}
		catch(IOException e)
	    {
	        System.out.println("Error with file operations");
	        e.printStackTrace();
	        System.exit(1); 
	    }
	    		closeFile(reader);	
	    		return content;
	}


	public void sendToFile(StringBuilder list, String file)
	{	
			try
			{
				writer = new BufferedWriter(new FileWriter(file));
			}
			catch(IOException e)
		    {
		        System.out.println("Error opening file");
		        e.printStackTrace();
		    }

				try
				{
					writer.write(list.toString());
				}
				catch(IOException e)
		    	{
			        System.out.println("Error with file operations");
			        e.printStackTrace();
			        System.exit(1); 
	    		}
			
			closeFile(writer);
	}

	

	public <T extends Closeable> void closeFile(T closer)
	{
		try
		    {
		      closer.close();
		    }
		    catch(IOException e)
		    {
	        	System.out.println("Error closing the file");
	        	System.exit(1);
		    }
	}
}

class Driver
{
	public static void main(String[] args) throws IOException {
		FileCompiler recur = new FileCompiler();
	}
}




























