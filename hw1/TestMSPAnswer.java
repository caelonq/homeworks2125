import org.junit.*;
import static org.junit.Assert.*; 


public class TestMSPAnswer
{
	 MSPAnswer answer;
	 int [] array = { 3, 6, 9, 12, -15, -18, -21, 24};
	 int [] dummyArray = { 3, 6, 9, 12, -15, -18, -21, 24};
	 MSPAnswer dummy;

	@Before
	public void initialize()
	{
		answer = new MSPAnswer();
		dummy = new MSPAnswer();
		
	}


	@Test
	public void testGetMaxSum()
	{	
		
		answer.mspaSum(array);

		assertEquals(answer.getMaxSum(), 30);
		assertFalse(answer.getMaxSum() < 24);
		assertTrue(answer.getMaxSum() == 30 );

	}


	@Test
	public void testGetStartingIndex()
	{
		answer.mspaSum(array);

		assertEquals(answer.getStartingIndex(), 0);
		assertFalse(answer.getStartingIndex() ==  3);
		assertTrue(answer.getStartingIndex() != 7 );

	}

	@Test
	public void testGetEndingIndex()
	{
		answer.mspaSum(array);
		
		assertEquals(answer.getEndingIndex(), 3);
		assertFalse(answer.getEndingIndex() == 7);
		assertTrue(answer.getEndingIndex() != 2 );

	}


	@Test
	public void testMSPAnswer()
	{
		
		dummy.mspaSum(dummyArray);
		answer.mspaSum(array);
		
		assertEquals(answer.toString(), dummy.toString());
		assertArrayEquals(dummyArray, array);
		assertEquals(dummy.getStartingIndex(), answer.getStartingIndex());
	}
}