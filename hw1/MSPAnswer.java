public class MSPAnswer
{
	
  private static MSPAnswer answer = new MSPAnswer();
  private static int maxSum, endingIndex, startingIndex;

  // Linear Runtime O(N)
  public static MSPAnswer mspaSum(int [] a)
  {
    int maxSum = 0, thisSum = 0, m = 0;


    for(int j = 0; j < a.length; ++j)
    {
      thisSum += a[j];

      if(thisSum > maxSum)
      {  
        maxSum = thisSum;
        endingIndex = j;
        startingIndex = m;
      } 
      else if(thisSum < 0)
      { 
        thisSum = 0;
        m = j + 1;
      }

    }
    answer.setMSPAnswer(maxSum, startingIndex, endingIndex);

    return answer;
  }


  private void setMSPAnswer(int maxSum, int startingIndex, int endingIndex)
  {
    this.maxSum = maxSum;
    this.startingIndex = startingIndex;
    this.endingIndex = endingIndex;
  }

  public int getMaxSum()
  {
    return this.maxSum;
  }

  public int getStartingIndex()
  {
    return this.startingIndex;
  }

  public int getEndingIndex()
  {
    return this.endingIndex;
  }


  public String toString()
  {
    return String.format("Max Sum: %d\nStarting Index: %d\nEnding Index: %d", 
      getMaxSum(), getStartingIndex(), getEndingIndex());
  }

}
