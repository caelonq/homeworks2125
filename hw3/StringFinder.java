
public class StringFinder
{
	public boolean findString(char ch1[], char ch2[])
	{
		int ch1LENGTH = ch1.length - 1;
		int ch2LENGTH = ch2.length;
		double ch1Hash = hash(ch1, 29);
		double ch2Hash = 0;
		if((ch1LENGTH + 1) <= ch2LENGTH)
			ch2Hash = hash(ch2, 0, (ch1LENGTH + 1), 29);
		else
			return false;
		int max = (int) ch2LENGTH - (ch1LENGTH + 1);

			for(int i = 0; i <= max; i++)
			{	

				if(ch1LENGTH < ch2LENGTH)
				{
				// Answer to number 1, it's constant time because it requires at most two operations
				// by incrementing the position of the search width by 1 X 1. The width is the search
				// string or the first char array (ch1) and 1 X 1 is (front X end) of the char array.
					ch2Hash = hash(ch2, i, ++ch1LENGTH, 29);
					if( ch1Hash == ch2Hash)
					{	
						System.out.println(i);
					// Answer to number 2, its O(k + N) because although the hash values are equal,
					// the stringCompare method makes O(N) comparisons checking both arrays character 
					// by character; and stringCompare is also called k times if i haven't reached its 
					// max value of the second array's length minus the first array's length, thus not N,
					// but K because i is the bounding value resricting both strings from being 
					// compared N times (the length of the second string).
						if( stringCompare(ch1, ch2, i) == true)
						{	
							return true;
						}
						else
						{	
							System.out.println("Strings Collided - 1st: "+new String(ch1)+" and 2nd: "+
								new String().valueOf(ch2, i, ch1.length));
							++ch1LENGTH;
						}
					}
				}
			}

					return false;

	}


	public boolean stringCompare(char ch1[], char ch2[], int l)
	{	
		for(int i = 0; i < ch1.length; ++i)
		{	
			if(l < ch2.length)
			{
				if(ch1[i] != ch2[l])
					return false;
				else
					++l;
			}
			else
				return false;
		}
		return true;
	}


	public double hash(char[] ch1, int start, int end, int size)
	{
		double hashVALUE = 0;

		for(int i = start; i < end; ++i)
		{	
			hashVALUE += (getLastDigit(getLastDigit(ch1[i]) * getLastDigitGetNextPlace(ch1[i]))
			* ch1[i]) + 127;
		}

		hashVALUE = hashVALUE % size;
		System.out.println(hashVALUE+" #2");
		if( getDecimal(hashVALUE) != 0)
			hashVALUE = (hashVALUE % size) * size;
		else 
			return hashVALUE;

		return hashVALUE;
	}


	public double hash(char[] ch1, int size)
	{
		double hashVALUE = 0;


		for(int i = 0; i < ch1.length; ++i)
		{	
			hashVALUE += (getLastDigit(getLastDigit(ch1[i]) * getLastDigitGetNextPlace(ch1[i]))
			* ch1[i]) + 127;
		}

		hashVALUE = hashVALUE % size;
		System.out.println(hashVALUE+" #1");
		if( getDecimal(hashVALUE) != 0)
			hashVALUE = (hashVALUE % size) * size;
		else 
			return hashVALUE;

		return hashVALUE;
	}
	private static int getLastDigit(int n)
	{	
		int digit = n;

		if ( n > 9 || n < -9)
		{
			n = 10 * ( n / 10);
			digit-=n;
		}

		return digit;
	}

	private static int getLastDigitGetNextPlace(int n)
	{
		int remainingDigit = n;

		if( n > 9 || n < -9)
		{
			remainingDigit -= getLastDigit(n);
			remainingDigit /= 10;
		}
		else
			remainingDigit = 0;

		return remainingDigit;
	}

	private static int numberOfDigits(int n)
	{
		int digits = 1;

		if(n > 0)
		{	
			digits += numberOfDigits(getLastDigitGetNextPlace(n));
		}

		return digits;
	}

	private static Double getDecimal(Double number)
	{	
		Double digit = number;
		int m = Integer.parseInt(String.format("%.0f", number));

		if ( number > 9 || number < -9)
		{
			 m = (int) (10 * ( number / 10));
			digit -= m;
			return getDecimal(digit);
		}
		else
			return (digit - m);
	}


}

}