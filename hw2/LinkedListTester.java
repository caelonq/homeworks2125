public class LinkedListTester{

    public static void main(String[] args) {


        LinkedList<String> myList = new LinkedList<String>();
        //LinkedList<String>.LinkedListIterator iter = myList.iterator();

        myList.insert("Ignatious",0);
        System.out.println(myList.size());
        myList.insert("Reilly",1);
        System.out.println(myList.size());
        myList.insert("J.",1);
        System.out.println(myList.size());


        System.out.print(myList.get(0));
        System.out.print(" ");
        System.out.print(myList.get(1));
        System.out.print(" ");
        System.out.print(myList.get(2));
        System.out.println();
        
        myList.insert("Dr.",0);
        System.out.print(myList.get(0));
        System.out.print(" ");
        System.out.print(myList.get(1));
        System.out.print(" ");
        System.out.print(myList.get(2));
        System.out.print(" ");
        System.out.print(myList.get(3));
        System.out.println();


    }


} // end class
