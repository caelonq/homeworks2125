import java.util.*;

public class LinkedList<AnyType> {

    // encapsulation!!!!
    private Node first;
    private Node last;
    private int   size;

    public LinkedList() {
        first = null;
        last  = null;
        size = 0;
    }

    public boolean isEmpty() {
        return this.size == 0;
    }

    public void insert(AnyType value, LinkedListIterator iter) {

        Node b = new Node(value);
        if(isEmpty() == true)
        {
            first = b;
            last = b;
            ++size;
        }    
        else if(iter.whereIAm.equals(first))
        {   
            b.setNext(iter.whereIAm);
            first = b;
            ++size;
        }
        else if(iter.whereIAm.equals(last))
        {
            last.setNext(b);
            last = b;
            iter.next();
            ++size;
        }    
        else
        {
            b.setNext(iter.whereIAm.getNext());
            iter.whereIAm.setNext(b);
            iter.next();
            ++size;
        }

    }

    public void insert(AnyType value, int index) {

    Node n = new Node(value);

    if (isEmpty() && index == 0) {
        this.first = n;
        this.last = n;
        size++;
    }        
    else if (size >= index && !isEmpty()) {
        if (index == 0 ){ // inserting at beginning
            n.setNext(this.first);
            this.first = n;
        }
        else if (index == size ) { // inserting at end
            this.last.setNext(n);
            this.last = n;
        }
        else if (index < size) { // inserting in middle
            // first, find the node "before" where we want to insert n
            Node before = first;
            int nodeNum = 0;
            while (nodeNum <= index-1) {
                before = before.getNext();
                nodeNum++;
            }
            // when we get to this point, before should have a reference to the node at index-1
            //System.out.println("NodeNum: " + nodeNum);
            //System.out.println("Node value: " + before.getElement());

            n.setNext(before.getNext());
            before.setNext(n);

        }
        size++;

    }
    else
        System.err.println("Error: cannot insert at that point");
    }

    public void delete(int index) {

        Node mark = this.first;

        if(isEmpty() == false)
        {    
            if(index == 0)
            {
                this.first = this.first.getNext();
            }
            else
            {
                while(mark.getNext().getElement() != get(index))
                {
                    mark = mark.getNext();
                }

                mark.setNext(mark.getNext().getNext());    
            }
        }        

    }    

    public void delete(LinkedListIterator iter){ 
        iter.remove();
    }    

    public AnyType get(int index) {
        Node result = first;
        int nodeNum = 0;
        while (nodeNum < index && result != null) {
            result = result.getNext();
            nodeNum++;
        }

        if(result == null){return null;}

        return result.getElement();
    }

    public AnyType get(LinkedListIterator iter) {
        return iter.whereIAm.getElement();
    }

    public LinkedListIterator iterator() {
        return this.new LinkedListIterator();
    }

    public int size() {
        return this.size;
    }

    public class LinkedListIterator implements java.util.Iterator<AnyType>{

        private Node whereIAm;

        public LinkedListIterator() {
            whereIAm = first;
        }

        // next returns the next element in the list, and increments
        // the iterator
        public AnyType next() {
            if (hasNext()) {
              AnyType value = whereIAm.getNext().getElement();
              whereIAm = whereIAm.getNext();
              return value;
            }
            else throw new java.util.NoSuchElementException( );
        }

        public boolean hasNext() {
            if (whereIAm.getNext() != null )
                return true;
            return false;
        }

        public void remove() {
            Node temp = first;

            if(whereIAm.equals(first))
            {
                first = first.getNext();
            }
            else
            {   
                /*If the temporary node's next
                    node is not where I Am, the
                temporary node advances one node closer to my previous node.*/
                while((temp.getNext() != whereIAm) && (whereIAm != null))  
                {   
                        temp = temp.getNext();
                }
                temp.setNext(whereIAm.getNext()); //Delete where I Am.
                whereIAm = temp; //I am now the previous node of the node deleted.
            }
               
        }
    }    // end class Iterator

    private class Node {

        private AnyType myDataElement;
        private Node next;

        public Node(AnyType data) {
            myDataElement = data;
            next = null;
        }

        public Node(AnyType data, Node next) {
            myDataElement = data;
            this.next = next;
        }

        public Node getNext() {
            return this.next;
        }

        public AnyType getElement() {
            return this.myDataElement;
        }

        public void setNext(Node next) {
            this.next = next;
        }
            
        public void setElement(AnyType data) {
            this.myDataElement = data;
        }

    } // end class Node

} // end class LinkedList
