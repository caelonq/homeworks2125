import org.junit.*;
import static org.junit.Assert.*; 
import java.lang.*;


public class LinkedListTester2
{
	LinkedList<Integer> myList;
	LinkedList<Integer>.LinkedListIterator iter; 
	Exception e;
	int[] array = {-2147483648, 2, 3, 4, 5, 6, 2147483647}; // Nodes can contain values MAX INT and MIN INT.

	@Test
	public void testIsEmpty()
	{
		assertFalse(myList.isEmpty());
	}

	@Before
	public void insertion()
	{	
		myList = new LinkedList<Integer>();
		Integer d = 0;
		//for(int y = 99999999; y <= 999999999; )			//Proper resources needed.
			//d*= y; 
		
		myList.insert(d, myList.iterator());


  		int j = 0;
  		for(int i: array)
  		{	
  			myList.insert(i, j);
  			++j;
  		}

  		assertFalse(myList.isEmpty());

  		for( int u = 0; u < myList.size(); ++ u)
  				System.out.println(myList.get(u));
	}
		

	@Test
	public void testDeleteandGet()
	{
		iter = myList.iterator();

		assertTrue(myList.get(0) == -2147483648);
		assertTrue( 3 == myList.get(3));
		assertEquals(2, (int) myList.get(2));

		iter.next();
		myList.delete(iter);
		assertEquals(2, (int) myList.get(1));

		while(iter.next() != myList.get(3))
		{
			//do nothing until I'm the node to be deleted.
		}
		myList.delete(iter);
		assertEquals((int)myList.get(3), 5);


	}

}