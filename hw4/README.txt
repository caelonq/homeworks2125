Command Line Example:
                                         Source  Destination
> java graph.FindShortestPath ny.co ny.gr  4       6 


A binary based queue is used for dijkstra's algorithm because it has good time complexities (theta log N) for node retrievals/removals. And constant time upper bounds for operations such as peek and size similar to an array. The queue is also used to print out the path because the Max-Heap used for the queue achieves (theta log N) for searching Nodes and constant time access for retrieving the largest weighted Node (destination node).