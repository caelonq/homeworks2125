package node;

public class Edge<T extends Node>
{
    private T node1;
    private T node2;
    private final int distance;

    public Edge(T node1,  T node2, int distance) //Edge is a pair of nodes, distance is the weight of the edge.
    {
        this.node1 = node1;
        this.node2 = node2;
        this.distance = distance;
    }

    public T getAdjacentNode(T node) //Get the opposite Node of the 'node' part of the Edge.
    {
        return node.getID() != node1.getID() ? node1 : node2; 
    }

    public int getDistance(){ return distance; } //Get the edge's distance/weight.

    public String toString() //String representation of the owner Node's distance to the opposite Node.
    {
        return String.format("\n\nNode %s ~> Node %s ::: Distance: %d\n\n", node1.toString(), node2.toString(), distance);
    }
}







