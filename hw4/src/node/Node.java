package node;
import java.util.Comparator; 

public interface Node
{
	void setData(Object data); //Data the node owns
	Object getData(); //Get the data
	Integer getID(); //Get the ID of the Node
	static Comparator<Object> comparator(){ return null; }; 
	default int getDistance(){return 0;}; //Distance of the Node along a path traversal
	default void setDistance(int x){}; //Set the Node's distance
	default Node getFrom(){return null;}; //Get the Node accessing this Node on a path
	void setFrom(Node n); //Set the node accessing this Node on a path
	
}