package node;
import java.util.Comparator;
import java.lang.Comparable;


public class Vertex implements Node, Comparable<Vertex>
{
    private final Integer ID; //Value/ID of the Node 
    private int distance = Integer.MAX_VALUE; //Distance is initially the MAXIMUM allowable ID
    private double longitude , latitude = 0; //ALT and LONG are initially zero
    private Object data = null; //Generic data the vertex can hold/record
    private Node from = null; //The Node accessing this Node part of an edge.

    public Vertex(int x){ ID = x; }

    public Vertex(int x, double lat, double lon)
    {
        this(x);
        latitude = lat;
        longitude = lon;
    }

    public Integer getID(){ return ID; }

    public int getDistance() { return distance; }

    public void setDistance(int dist) { distance = dist; }

    public void setFrom(Node n){ from = n; }

    public Node getFrom(){ return from; }
    

    public boolean equals(Vertex vertex){ return vertex.getID() == ID; }

    public double getLatitude(){ return latitude; }

    public double getLongitude(){ return longitude; }

    public void setData(Object data){ this.data = data; }

    public Object getData(){ return data; }

    public String toString()
    {
        return String.format("\nNode %d Lat %10.5f Lon %10.5f Distance %5d\n", ID, latitude, longitude, distance);
    }

    @Override
    public int compareTo(Vertex v)
    {
        if (this.distance > v.getDistance())
            {
                return 1;
            } 
        if(this.distance < v.getDistance())
            {
                return -1;
            }

                return 0;
    }
    public static Comparator<Vertex> comparator(){ return new VertexComparator(); } 
    public static Comparator<Vertex> comparatorInv(){ return new VertexComparatorInv(); }


    static class VertexComparator implements Comparator<Vertex> //Least to greatest compare
    {
        public int compare(Vertex v1, Vertex v2)
        {

            return v1.compareTo(v2);
        }
    }

    static class VertexComparatorInv implements Comparator<Vertex> //Greatest to least compare
    {
        public int compare(Vertex v1, Vertex v2)
        {

            return v2.compareTo(v1);
        }
    }
}