package graph;
import node.*;
import java.util.*;

public class Graph<T extends Node>
{
    private HashMap<T, List<Edge<T>>> map; //Map containing a Node and its edges
    private int numberOfVertices; //Number of vertices the graph will contain


    Graph(int numberOfVert) //Graph x number of vertices
    {
        if (numberOfVert < 0) //Number of vertices must exist in the set of natural #s
        {
            throw new IllegalArgumentException("Number of vertices can't be less than zero");
        }

        numberOfVertices = numberOfVert; //Declaration of the number of vertices

        map = new HashMap<T, List<Edge<T>>>(numberOfVert, (float) .4); //Map the Nodes to their edges; the size of the map is the number of vertices; load factor of .4 for never allowing the data structure to be half-full. 
    }

    public void addEdge(T node1, T node2, int distance) //Two nodes make an edge whereby node1/node2 owns the edge. But the .gr file accounts for shared ownership so only the argument to node1 needs to own the edge.
    {

        if (node1 == null || node2 == null) //Can't have an edge if there aren't at least two endpoints so throw an exception.
        {
            throw new NullPointerException("Node is null");
        }
        if (distance < 0) // In the context of traveling the distance traveled cannot be a negative number, so throw an exception.
        {
            throw new IllegalArgumentException(" Edges can't have negative weights in Dijkstra's Alg ");
        }

        Edge<T> edge = new Edge<T>(node1, node2, distance);

        addToMap(node1, edge); //addToMap(node2, edge); If the gr file didn't account for shared ownership;
    }

    private void addToMap(T node, Edge<T> edge) // Adds the edge to the list of edges owned by 'node'; create a edge list and adds the Node/edgeList pair to the map.
    {

        if (map.containsKey(node)) //If the map contains the Node retrive its adjacency list and add the edge.
        {
            List<Edge<T>> l = map.get(node);
            l.add(edge);
        }
        else 
        {
            List<Edge<T>> l = new ArrayList<Edge<T>>(); //If not make a list and then add the edge to the list and map the list to the Node.
            l.add(edge);
            map.put(node, l);
        }
    }

    public  List<Edge<T>> getAdj(T node){ return map.get(node); } //Get the adjaceny list of Node

    public Map<T , List<Edge<T>>> getGraph(){ return map; } //Get the map of Nodes and their list of edges

    public int getNumVertices(){ return numberOfVertices; } //Get the number of vertices.

    public String toString(){ return map.toString(); } //Returns the string representation of the map.
}