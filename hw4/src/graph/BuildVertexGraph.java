package graph;
import java.util.Scanner;
import algorithm.Dijkstra;
import node.*;
import java.io.*;

public class BuildVertexGraph
{
	private static File f;
	private static Scanner fileReader;
	private static String line;
	private static Vertex[] vertices; //Array of vertices
    private Graph<Vertex> graph;
    private static int n; //Holds the number of vertices

	public Vertex[] initVertices(String file)throws FileNotFoundException
	{
		

        File f = new File(file); //File containing four-column format - token-identifier|ID|Lat|Long and 5-column format with the 5th token containing the number of vertices.
        fileReader = new Scanner(f);
        line = "";

        while(fileReader.hasNextLine())
        {

            line = fileReader.nextLine();
            String[] tokens = line.split(" "); //Tokenize the input

            if(tokens.length == 5)
            if(tokens[3].equals("co")) //Token-identifier
            {
                n = Integer.parseInt(tokens[4]); //Number of Vertices
                vertices = new Vertex[n+1]; //Array of all the Vertices
            }
            if(tokens[0].equals("v")) //Token-identifer for vertices
            { 
            /******************************************************************************************************************/
                String ID = tokens[1]; 
                double curLat = Double.parseDouble(tokens[3]);
                curLat = curLat * 0.000001; 							/** Author: Aaron Maus */
                double curLon = Double.parseDouble(tokens[2]);
                curLon = curLon * 0.000001;
                int key = Integer.parseInt(ID);
                vertices[key] =  new Vertex(key, curLat, curLon);
            /******************************************************************************************************************/
            }
        }

        return vertices;
    }

	public Graph<Vertex> initGraph(String file)throws FileNotFoundException
	{   
        graph = new Graph<Vertex>(n+1); //Make the graph
		f = new File(file); //File containing four-column format - token-identifier|ID|ID|weight/distance
	    fileReader = new Scanner(f);
	    line = "";
	    while(fileReader.hasNextLine())
        {
            line = fileReader.nextLine();
            String[] tokens = line.split(" ");
            if(tokens[0].equals("a")) //Token-identifer
            {   
                //edges created from ID|ID|weight/distance
            	graph.addEdge(vertices[Integer.parseInt(tokens[1])], vertices[Integer.parseInt(tokens[2])], Integer.parseInt(tokens[3]));
            }
     	}

     	return graph; //return the created Graph.
	}

}