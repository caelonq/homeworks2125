package graph;
//import graph.BuildVertexGraph;
//import graph.Graph;
import algorithm.Dijkstra;
import node.Vertex;
import java.util.*;
import java.io.FileNotFoundException;

public class FindShortestPath
{	
	private static BuildVertexGraph Vgraph = new BuildVertexGraph();

	//args[0] = *.co; args[1] = *.gr; args[2] = source; args[3] = destination
	public static void main(String[] args)throws FileNotFoundException 
	{
		int _N_V_ = Vgraph.initVertices(args[0]).length; //Array of vertices to graph; _N_V_ =  number of vertices
		Graph<Vertex> graph = Vgraph.initGraph(args[1]); //Graph
		Dijkstra<Vertex> find = new Dijkstra<Vertex>(graph); //Handle for Dijkstra

		Object[] array = find.findShortestPath(Integer.parseInt(args[2]), Integer.parseInt(args[3])); //array of the vertices on the path from source to destination
		printShortestPath(array); //Display the shortest path.
	}



	public static void printShortestPath(Object [] ar) // Bottom up
	{
		PriorityQueue<Vertex> q = new PriorityQueue<Vertex>(ar.length+1, Vertex.comparatorInv()); //Max-heap queue

		for(Object i: ar)
		{
			q.add((Vertex) i);
		}
		Vertex node_ = q.peek(); //Get the destination/leaf Vertex.
		while(node_.getFrom() != null) //Traverse the path of the Vertices back the source/root Vertex whose accessed by no Vertex - null.
		{   
			System.out.print(" " + node_.toString()+ " ");
			node_ = (Vertex) node_.getFrom(); //Vertex is the parent vertex.
			
		}
		System.out.print("\n" + node_.toString()+ "\n"); //Prints the root/source Vertex.
	} 
}