package algorithm;
import node.Vertex;
import node.Edge;
import node.Node;
import graph.Graph;
import java.util.*;

public class Dijkstra<T extends Node>
{
    /*********************************************************************
                        *Tried to keep it Generic* (as t-shirt) as possible
    *********************************************************************/

    private Graph<T> graph; 

    public Dijkstra(Graph<T> graph)
    {
        if (graph == null) //There has to be graph to run dijkstra on.
        {
            throw new NullPointerException("The graph can't be null.");
        }
        this.graph = graph;
    }

    public Object[] findShortestPath(Integer source, Integer destination){ return findShortestPath(source, destination, 49); }//Helper method


    private Object[] findShortestPath(Integer source, Integer destination, Integer sizeOfQueue)// Find the shortest path from Nodes with IDs source and destination
    {
        PriorityQueue<T> queue = new PriorityQueue<T>(sizeOfQueue, T.comparator()); //Queue to maintain lowest weight Nodes
        PriorityQueue<T>  pathSet = new PriorityQueue<T>(sizeOfQueue, T.comparator());//Queue to maintain the path
        T destNode = null; //Destination Node is initially null
        T srcNode = null; // Source Node is initially null
        T node_ = null; //Key Node for retriving the ID of Nodes part of the graph.
        Integer newDistance = 0; //Keep track of the distances


        for (Map.Entry<T, List<Edge<T>>> entry :  graph.getGraph().entrySet())//Get each Node/edgelist pair
        {
                node_ = entry.getKey(); //Get the Node
            
            if (node_.getID() == source) //If the Node is the source then its distance is 0; add the node to the queue.
            {   
                srcNode = node_;
                srcNode.setDistance(0);
                queue.add(srcNode);
                source = -1;
            }
            if(srcNode != null && srcNode.getID().equals(destination)) //If the srcNode is the destination return a one-element array with the Node.
            {   
                Object[] a = {node_};
                return a;
            } 
            if(node_.getID() == destination) //If the Node is the destination set the destNode.
            {   
                destNode = node_;
                destination = -1;
            }
            if(destination == -1 && source == -1) //The source and destination variables are no longer needed after setting the src and dest Nodes, if they're set to -1 exit the for loop; The src and dest Nodes have been found.
                break;
        }

            while (!pathSet.contains(destNode)) //Get the Nodes along the path so long as the destination hasn't been reached
            {   
                if(queue.isEmpty() == false) //If the queue is not empty get the head of the queue
                {
                    T node = queue.poll(); //Head of the queue - Node with lowest weight.
                    if(!pathSet.contains(node))//If the pathSet doesn't contain the node at head of the queue add it.
                    {
                        pathSet.add(node); //Add the node to the pathset since its along the path and it now contains its lowest weight.
                        if(graph.getAdj(node) != null) //So long as there are edges to the Node get the list of edges.
                        {
                            for (Edge<T> edge : graph.getAdj(node)) //Cycle through all the Node's edges.
                            {   
                                T pathNode = edge.getAdjacentNode(node); //Get the other Node of the edge

                                if (!pathSet.contains(pathNode)) // If the pathSet doesn't contain the Node visit the Node
                                {
                                    newDistance = node.getDistance() + edge.getDistance(); //Distance of the observing Node and the adjacent edge's distance

                                    if (newDistance < pathNode.getDistance()) //If the newDistance is less than the adjacent Node's distance update the adjacent Node's distance with the new distance. Add the node to the queue.
                                    {   
                                        pathNode.setFrom(node); //Update the Node's record of where it was accessed from.
                                        pathNode.setDistance(newDistance); //Update the Node's new distance
                                        queue.add(pathNode); //Add the Node to the queue
                                    }
                                    pathNode.setFrom(node); //Update the Node's record of where it was accessed from.
                                }
                            }
                        }
                    }
                }
            }
        return pathSet.toArray(); //Return an Object array of shortest path items connected to the path of the source to the destination.
    }
}